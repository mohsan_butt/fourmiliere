﻿using System.Collections.Generic;
using LibAbstraite.GestionEnvironnement;
using LibAbstraite.GestionPersonnages;
using LibAbstraite.GestionEtape;

namespace LibAbstraite.Strategy
{
    public abstract class Deplacement
    {
        public PersonnageAbstrait Personnage { get; set; }

        protected Deplacement(PersonnageAbstrait personnageAbstrait)
        {
            Personnage = personnageAbstrait;
        }


        public abstract ZoneAbstraite NextPath(List<ZoneAbstraite> zones, Etape Etape);
    }
}