﻿using System.Collections.Generic;
using System.Linq;
using LibAbstraite.GestionEnvironnement;
using LibAbstraite.GestionPersonnages;
using LibAbstraite.GestionEtape;

namespace LibAbstraite.Strategy
{
    public class NoDeplacement : Deplacement
    {
        public NoDeplacement(PersonnageAbstrait personnageAbstrait) : base(personnageAbstrait)
        {
        }

        public override ZoneAbstraite NextPath(List<ZoneAbstraite> zone, Etape Etape)
        {
            return Personnage.Position;
        }

      
    }
}