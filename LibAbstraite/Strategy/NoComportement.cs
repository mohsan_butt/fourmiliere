﻿using System;
using LibAbstraite.GestionObjets;
using LibAbstraite.GestionPersonnages;

namespace LibAbstraite.Strategy
{
    public class NoComportement : Comportement
    {
        public NoComportement(PersonnageAbstrait personnageAbstrait) : base(personnageAbstrait)
        {
        }
    }
}