﻿using System.Security.Policy;
using LibAbstraite.GestionEnvironnement;
using LibAbstraite.GestionObjets;
using LibAbstraite.GestionPersonnages;
using LibAbstraite.GestionEtape;

namespace LibAbstraite.Strategy
{
    public abstract class Comportement
    {
        public PersonnageAbstrait Personnage { get; set; }

        protected Comportement(PersonnageAbstrait personnageAbstrait)
        {
            Personnage = personnageAbstrait;
        }

        public virtual void ExecuteFourmi(Etape Etape) { }
        public virtual void ExecuteObject(ZoneAbstraite zone, Position Home, Etape Etape) {}
    }
}