﻿using LibAbstraite.Xml;
using System;
using System.Xml;

namespace LibAbstraite
{
    public class Position: XmlData
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Position()
        {
           
        }

        public void Save(XmlNode rootNode, XmlWriterHelper helper)
        {
            helper.WriteAttribute(rootNode, "X", X.ToString());
            helper.WriteAttribute(rootNode, "Y", Y.ToString());
        }

        public void Hydrate(XmlNode rootNode, XmlReaderHelper helper)
        {
            X = int.Parse(helper.GetAttribute(rootNode, "X"));
            Y = int.Parse(helper.GetAttribute(rootNode, "Y"));
        }

        public string GetNodeName()
        {
            return "POSITION";
        }
    }
}
