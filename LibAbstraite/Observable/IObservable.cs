﻿namespace LibAbstraite.Observable
{
    public interface IObservable
    {
        void NotifyObservers();

    }

}