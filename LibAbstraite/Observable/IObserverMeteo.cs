﻿namespace LibAbstraite.Observable
{
    public enum Meteo
    {
        Soleil,
        Averse,
        Orage,
        Neige,
    }
    
    
    public interface IObserverMeteo
    {
        void Update(Meteo meteo);
  
    }
}