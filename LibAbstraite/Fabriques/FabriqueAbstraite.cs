﻿using System;
using System.Collections.Generic;
using System.Text;
using LibAbstraite.GestionEnvironnement;
using LibAbstraite.GestionObjets;
using LibAbstraite.GestionPersonnages;
using LibAbstraite.GestionEtape;

namespace LibAbstraite.Fabriques
{
    public abstract class FabriqueAbstraite
    {

        public abstract string Title { get; }

        public abstract EnvironnementAbstrait CreerEnvironment();
        public abstract ZoneAbstraite CreeZone(ZoneAbstraite.Type type, string name);

        public abstract AccesAbstrait CreeAccees(ZoneAbstraite zdebut, ZoneAbstraite zfin);

        public abstract PersonnageAbstrait CreePersonnage(PersonnageAbstrait.Type type);

        public abstract ObjetAbstrait CreeObjet(ObjetAbstrait.Type type);

        public abstract EventAbstrait CreeEvent(EventAbstrait.Type type);
    }
}
