﻿using System;
using System.Collections.Generic;
using System.Xml;
using LibAbstraite.Fabriques;
using LibAbstraite.GestionEnvironnement;
using LibAbstraite.GestionObjets;
using LibAbstraite.Observable;
using LibAbstraite.Strategy;
using LibMetier;
using LibAbstraite.Xml;
using LibAbstraite.GestionEtape;

namespace LibAbstraite.GestionPersonnages
{
    public abstract class PersonnageAbstrait: XmlData
    {
        public string Nom { get; set; }

        public double Sante { get; set; }

        private Random Hasard { get; set; }

        public ZoneAbstraite Position { get; set; }
        public Position Destination { get; set; }
        
        public Comportement Behavior { get; set; }
        public Deplacement FindPath { get; set; }

        public Meteo Meteo { get; set; }

        public List<ObjetAbstrait> ListStock { get; set; }

        public State CurrentState { get; set; }

        public bool IsDead { get; set; }

        public int NumberOfElementFind { get; set; }
        public int NumberOfAttack { get; set; }

        public abstract ZoneAbstraite ChoixZoneSuivante(List<ZoneAbstraite> accesList, Etape Etape);

        public abstract void AnalyseSituation();
        public abstract void Execution(Position Home, Etape Etape);

        public virtual void Update(Meteo meteo) { }

        protected PersonnageAbstrait() : this("Anonyme")
        {
        }

        protected PersonnageAbstrait(string name)
        {
            Nom = name;
            Behavior = new NoComportement(this);
            FindPath = new NoDeplacement(this);
            ListStock = new List<ObjetAbstrait>();
            IsDead = false;
            NumberOfElementFind = 0;
            NumberOfAttack = 0;
        }

        public void BaisserSante()
        {
            if (Sante == 0)
            {
                IsDead = true;
            }
            else
            {
                Sante -= 20;
            }
            
        }

        public virtual void Save(XmlNode rootNode, XmlWriterHelper helper)
        {
            helper.WriteAttribute(rootNode, "Nom", Nom);
            helper.WriteAttribute(rootNode, "Sante", Sante.ToString());
            helper.WriteListXmlData<ObjetAbstrait>(rootNode, "ListStock", "Stock", ListStock);
            helper.WriteAttribute(rootNode, "CurrentState", CurrentState.ToString());
            helper.WriteAttribute(rootNode, "IsDead", IsDead ? "1" : "0");
            helper.WriteXmlData(rootNode, "Destination", Destination);
        }

        public virtual void Hydrate(XmlNode rootNode, XmlReaderHelper helper)
        {
            Nom = helper.GetAttribute(rootNode, "Nom");
            Sante = int.Parse(helper.GetAttribute(rootNode, "Sante"));
            ListStock = helper.GetListXmlDataFabriqueObjet<ObjetAbstrait>(rootNode, "ListStock", "Stock");
            CurrentState = helper.GetAttributeAsEnum<PersonnageAbstrait.State>(rootNode, "CurrentState");
            IsDead = int.Parse(helper.GetAttribute(rootNode, "IsDead")) == 1;
            Destination = helper.GetXmlData<Position>(rootNode, "Destination", new Position());
            //Todo: Set Destination
        }

        public virtual string GetNodeName()
        {
            throw new NotImplementedException();
        }

        public enum Type
        {
            FOURMI, REINE, CEUILLEUSE,
            COMBATTANT
        }

        public enum State
        {
            Normale,
            Mourrant,
            Evolution,
            Mort
        }

        
    }
}