﻿namespace LibMetier.Command
{
    public interface Command
    {
        void execute();
    }
}