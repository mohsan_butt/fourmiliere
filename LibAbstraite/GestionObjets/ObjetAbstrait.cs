﻿using System;
using System.Xml;
using LibAbstraite.GestionEnvironnement;
using LibAbstraite.Xml;

namespace LibAbstraite.GestionObjets
{
    public abstract class ObjetAbstrait: XmlData
    {

        public string NodeName
        {
            get { return GetNodeName(); }
        }

        public string Nom { get; set; } 

        public ZoneAbstraite Position;

        public ObjetAbstrait() { }

        public enum Type
        {
            OEUF, NOURRITURE, PHEROMONE
        }

        public virtual void Save(XmlNode rootNode, XmlWriterHelper helper)
        {
            helper.WriteAttribute(rootNode, "Nom", Nom);
        }

        public virtual void Hydrate(XmlNode rootNode, XmlReaderHelper helper)
        {
            Nom = helper.GetAttribute(rootNode, "Nom");
        }

        public virtual string GetNodeName()
        {
            throw new NotImplementedException();
        }
    }
}