﻿using System.Xml;

namespace LibAbstraite.Xml
{
    public interface XmlData
    {
        void Save(XmlNode rootNode, XmlWriterHelper helper);
        void Hydrate(XmlNode rootNode, XmlReaderHelper helper);
        string GetNodeName();
    }
}