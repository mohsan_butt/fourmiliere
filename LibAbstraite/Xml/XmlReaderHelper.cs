﻿using LibAbstraite.Fabriques;
using LibAbstraite.GestionEnvironnement;
using LibAbstraite.GestionEtape;
using LibAbstraite.GestionObjets;
using LibAbstraite.GestionPersonnages;
using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace LibAbstraite.Xml
{
    public class XmlReaderHelper
    {
        public XmlDocument XmlDocument { get; set; }
        private FabriqueAbstraite Fabrique { get; set; }

        public XmlDocument Open(Stream stream, FabriqueAbstraite Fabrique)
        {
            XmlDocument = new XmlDocument();
            XmlDocument.Load(stream);
            this.Fabrique = Fabrique;
            return XmlDocument;
        }

        private T Hydrate<T>(XmlNode node, T t) where T : XmlData
        {
            if (GetAttribute(node, "obj__") == "null")
            {
                return default(T);
            }
            t.Hydrate(node, this);
            return t;
        }

        public string GetAttribute(XmlNode rootNode, string key)
        {
            return rootNode.Attributes[key].Value;
        }

        public T GetAttributeAsEnum<T>(XmlNode rootNode, string key) where T : struct
        {
            return (T)Enum.Parse(typeof(T), GetAttribute(rootNode, key));
        }

        public string GetXmlData(XmlNode rootNode, string key)
        {
            return rootNode.Attributes[key].Value;
        }

        public T GetXmlData<T>(XmlNode rootNode, string key, T t) where T: XmlData
        {
            if (t != null)
            {
                XmlNode childNode = rootNode.SelectSingleNode(key);
                t = Hydrate<T>(childNode, t);
            }
            return t;
        }

        public List<T> GetXmlListData<T>(XmlNode rootNode, string groupKey) where T : XmlData, new()
        {
            List<T> r = new List<T>();
            XmlNode listNode = rootNode.SelectSingleNode(groupKey);
            for (int i = 0; i < listNode.ChildNodes.Count; i++)
            {
                T t = new T();
                t = Hydrate<T>(listNode.ChildNodes.Item(i), t);
                r.Add(t);
            }
            return r;
        }


        //Environnement
        public T GetXmlDataFabriqueEnvironnement<T>(XmlNode rootNode, string key) where T : EnvironnementAbstrait, XmlData
        {
            XmlNode childNode = rootNode.SelectSingleNode(key);
            EnvironnementAbstrait env = Fabrique.CreerEnvironment();
            env = Hydrate<T>(childNode, (T)env);
            return (T)env;
        }

        public List<T> GetListXmlDataFabriqueEnvironnement<T>(XmlNode rootNode, string groupKey, string key) where T : EnvironnementAbstrait, XmlData
        {
            List<T> r = new List<T>();
            XmlNode listNode = rootNode.SelectSingleNode(groupKey);
            for (int i = 0; i < listNode.ChildNodes.Count; i++)
            {
                r.Add(GetXmlDataFabriqueEnvironnement<T>(listNode.ChildNodes.Item(i), key));
            }
            return r;
        }





        //Zone
        public T GetXmlDataFabriqueZone<T>(XmlNode rootNode, string key) where T : ZoneAbstraite, XmlData
        {
            XmlNode childNode = rootNode.SelectSingleNode(key);
            return GetXmlDataFabriqueZonePrivate<T>(childNode, key);
        }

        public List<T> GetListXmlDataFabriqueZone<T>(XmlNode rootNode, string groupKey, string key) where T : ZoneAbstraite, XmlData
        {
            List<T> r = new List<T>();
            XmlNode listNode = rootNode.SelectSingleNode(groupKey);
            foreach (var childNode in listNode.ChildNodes)
            {
                r.Add(GetXmlDataFabriqueZonePrivate<T>((XmlNode) childNode, key));
            }
            return r;
        }

        private T GetXmlDataFabriqueZonePrivate<T>(XmlNode childNode, string key) where T : ZoneAbstraite, XmlData
        {
            ZoneAbstraite zone = Fabrique.CreeZone(GetAttributeAsEnum<ZoneAbstraite.Type>(childNode, "obj__"), "");
            zone = Hydrate<T>(childNode, (T)zone);
            return (T)zone;
        }




        //Objet
        public T GetXmlDataFabriqueObjet<T>(XmlNode rootNode, string key) where T : ObjetAbstrait, XmlData
        {
            XmlNode childNode = rootNode.SelectSingleNode(key);
            return GetXmlDataFabriqueObjetPrivate<T>(childNode, key);
        }

        public List<T> GetListXmlDataFabriqueObjet<T>(XmlNode rootNode, string groupKey, string key) where T : ObjetAbstrait, XmlData
        {
            List<T> r = new List<T>();
            XmlNode listNode = rootNode.SelectSingleNode(groupKey);
            for (int i = 0; i < listNode.ChildNodes.Count; i++)
            {
                r.Add(GetXmlDataFabriqueObjetPrivate<T>(listNode.ChildNodes.Item(i), key));
            }
            return r;
        }

        private T GetXmlDataFabriqueObjetPrivate<T>(XmlNode childNode, string key) where T : ObjetAbstrait, XmlData
        {
            
            ObjetAbstrait obj = Fabrique.CreeObjet(GetAttributeAsEnum<ObjetAbstrait.Type>(childNode, "obj__"));
            obj = Hydrate<T>(childNode, (T)obj);
            return (T)obj;
        }



        //Personnage
        public T GetXmlDataFabriquePersonnage<T>(XmlNode rootNode, string key) where T : PersonnageAbstrait, XmlData
        {
            XmlNode childNode = rootNode.SelectSingleNode(key);
            return GetXmlDataFabriquePersonnagePrivate<T>(childNode, key);
        }

        public List<T> GetListXmlDataFabriquePersonnage<T>(XmlNode rootNode, string groupKey, string key) where T : PersonnageAbstrait, XmlData
        {
            List<T> r = new List<T>();
            XmlNode listNode = rootNode.SelectSingleNode(groupKey);
            for (int i = 0; i < listNode.ChildNodes.Count; i++)
            {
                r.Add(GetXmlDataFabriquePersonnagePrivate<T>(listNode.ChildNodes.Item(i), key));
            }
            return r;
        }

        public T GetXmlDataFabriquePersonnagePrivate<T>(XmlNode childNode, string key) where T : PersonnageAbstrait, XmlData
        {
            PersonnageAbstrait personnage = Fabrique.CreePersonnage(GetAttributeAsEnum<PersonnageAbstrait.Type>(childNode, "obj__"));
            personnage = Hydrate<T>(childNode, (T)personnage);
            return (T)personnage;
        }


        //Event
        public T GetXmlDataFabriqueEvent<T>(XmlNode rootNode, string key) where T : EventAbstrait, XmlData
        {
            XmlNode childNode = rootNode.SelectSingleNode(key);
            return GetXmlDataFabriqueEventPrivate<T>(childNode, key);
        }

        public List<T> GetListXmlDataFabriqueEvent<T>(XmlNode rootNode, string groupKey, string key) where T : EventAbstrait, XmlData
        {
            List<T> r = new List<T>();
            XmlNode listNode = rootNode.SelectSingleNode(groupKey);
            for (int i = 0; i < listNode.ChildNodes.Count; i++)
            {
                r.Add(GetXmlDataFabriqueEventPrivate<T>(listNode.ChildNodes.Item(i), key));
            }
            return r;
        }

        public T GetXmlDataFabriqueEventPrivate<T>(XmlNode childNode, string key) where T : EventAbstrait, XmlData
        {
            EventAbstrait Event = Fabrique.CreeEvent(GetAttributeAsEnum<EventAbstrait.Type>(childNode, "obj__"));
            Event = Hydrate<T>(childNode, (T)Event);
            return (T)Event;
        }
    }
}