﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml;

namespace LibAbstraite.Xml
{
    public class XmlWriterHelper
    {

        public XmlDocument XmlDocument { get; set; }

        public XmlDocument CreateDocument()
        {
            XmlDocument = new XmlDocument();
            return XmlDocument;
        }


        public void WriteXmlData<T>(XmlNode rootNode, string key, T data, Action<XmlNode> modifier = null) where T : XmlData
        {
            XmlNode node = XmlDocument.CreateElement(key);
            WriteAttribute(node, "obj__", data == null ? "null" : data.GetNodeName());
            modifier?.Invoke(node);
            if (data != null)
                data.Save(node, this);
            rootNode.AppendChild(node);
        }


        public void WriteListXmlData<T>(XmlNode rootNode, string groupKey, string key, IList<T> data_, Action<XmlNode> modifier = null) where T: XmlData
        {
            XmlNode node = XmlDocument.CreateElement(groupKey);
            foreach (var data in data_)
            {
                WriteXmlData(node, key, data, modifier);
            }
            rootNode.AppendChild(node);
        }

        public void WriteAttribute(XmlNode node, string key, string value)
        {
            XmlAttribute attribute = XmlDocument.CreateAttribute(key);
            attribute.Value = value;
            node.Attributes.Append(attribute);
        }


        public void SaveAs(Stream stream)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            XmlWriter writer = XmlWriter.Create(stream, settings);
            XmlDocument.Save(writer);
        }
    }
}