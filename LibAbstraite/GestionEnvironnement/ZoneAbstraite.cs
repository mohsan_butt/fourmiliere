﻿using System;
using System.Collections.Generic;
using System.Xml;
using LibAbstraite.Fabriques;
using LibAbstraite.GestionObjets;
using LibAbstraite.GestionPersonnages;
using LibMetier;
using LibAbstraite.Xml;

namespace LibAbstraite.GestionEnvironnement
{
    public abstract class ZoneAbstraite: XmlData
    {
        public string Nom { get; set; }

        public Position Coordonne { get; set; }
 
        public List<ObjetAbstrait> ObjectList { get; set; }

        public List<PersonnageAbstrait> PersonnageList { get; set; }

        public List<AccesAbstrait> AccesAbstraitList;

        public bool IsZoneFourmiliere { get; set; }

        protected ZoneAbstraite()
        {
            ObjectList = new List<ObjetAbstrait>();
            PersonnageList = new List<PersonnageAbstrait>();
            AccesAbstraitList = new List<AccesAbstrait>();
            Coordonne = new Position();
            IsZoneFourmiliere = false;
        }

        public void AjouteAcces(AccesAbstrait acces)
        {
            AccesAbstraitList.Add(acces);
        }

        public void AjouterObjet(ObjetAbstrait objet)
        {
            ObjectList.Add(objet);
        }

        public void AjouterPersonnage(PersonnageAbstrait unPersonnage)
        {
            PersonnageList.Add(unPersonnage);
        }

        public void RetirerPersonnage(PersonnageAbstrait unPersonnage)
        {
            PersonnageList.Remove(unPersonnage);
        }

        public ZoneAbstraite(string unNom, int x, int y)
        {
            Nom = unNom;
            ObjectList = new List<ObjetAbstrait>();
            PersonnageList = new List<PersonnageAbstrait>();
            AccesAbstraitList = new List<AccesAbstrait>();
            Coordonne = new Position(x, y);
        }

        protected ZoneAbstraite(string unNom)
        {
            Nom = unNom;
            ObjectList = new List<ObjetAbstrait>();
            PersonnageList = new List<PersonnageAbstrait>();
            AccesAbstraitList = new List<AccesAbstrait>();
            Coordonne = new Position();
        }

        public override string ToString()
        {
            return Nom;
        }

        public virtual void Save(XmlNode rootNode, XmlWriterHelper helper)
        {
            helper.WriteAttribute(rootNode, "Nom", Nom);
            helper.WriteXmlData(rootNode, "Coordonne", Coordonne);
            helper.WriteListXmlData(rootNode, "ObjectList", "ObjectObj", ObjectList);
            helper.WriteListXmlData(rootNode, "PersonnageList", "PersonnageObj", PersonnageList);
        }

        public virtual void Hydrate(XmlNode rootNode, XmlReaderHelper helper)
        {
            Nom = helper.GetAttribute(rootNode, "Nom");
            Coordonne = helper.GetXmlData<Position>(rootNode, "Coordonne", new Position());
            ObjectList = helper.GetListXmlDataFabriqueObjet<ObjetAbstrait>(rootNode, "ObjectList", "ObjectObj");
            PersonnageList = helper.GetListXmlDataFabriquePersonnage<PersonnageAbstrait>(rootNode, "PersonnageList", "PersonnageObj");
        }

        public virtual string GetNodeName()
        {
            throw new NotImplementedException();
        }

        public abstract void MediationConflit();

        public enum Type
        {
            BOUTDETERRAIN,
            ZONEFOURMILIERE
        }
    }
}