﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml;
using LibAbstraite.Fabriques;
using LibAbstraite.GestionObjets;
using LibAbstraite.GestionPersonnages;
using LibAbstraite.Xml;
using LibAbstraite.GestionEtape;

namespace LibAbstraite.GestionEnvironnement
{
    public abstract class EnvironnementAbstrait: XmlData
    {

        public int DimensionX { get; set; }
        public int DimensionY { get; set; }

        public ObservableCollection<PersonnageAbstrait> PersonnageList { get; set; }
        public List<ObjetAbstrait> ObjetsList { get; set; }

        //public List<ZoneAbstraite> ZoneAbstraitList;
        public ZoneAbstraite [,] ZoneAbstraitList { get; set; }
        public List<AccesAbstrait> AccesAbstraitList;

        public List<Etape> Etapes { get; private set; }
        protected int EtapeCount = 0;

        public Position Home { get; set; }

        /**
         *  Ajouter la fabrique en paramète génére une reeur
         *  
         * */
        protected void AjouterChemin(FabriqueAbstraite fabrique ,params AccesAbstrait[] accessArray)
        {
            foreach (AccesAbstrait acces in accessArray)
            {
                AccesAbstraitList.Add(acces);
            }
        }


        public void AjouterObjet(ObjetAbstrait unObjet)
        {
            ObjetsList.Add(unObjet);
           var hasard = new Random();
           int x = hasard.Next(DimensionX);
           int y = hasard.Next(DimensionY);
           ZoneAbstraitList[x,y].AjouterObjet(unObjet);
           unObjet.Position = ZoneAbstraitList[x,y];
        }

        public void AjouterPersonage(PersonnageAbstrait unPersonnage)
        {
            var hasard = new Random();
            int x = hasard.Next(DimensionX);
            int y = hasard.Next(DimensionY);
            ZoneAbstraitList[x,y].AjouterPersonnage(unPersonnage);
            unPersonnage.Position = ZoneAbstraitList[x,y];
            PersonnageList.Add(unPersonnage);
        }

        public virtual void AjouterZoneAbstrait(ZoneAbstraite zoneAbstrait, int x, int y)
        {
            zoneAbstrait.Coordonne.X = x;
            zoneAbstrait.Coordonne.Y = y;
            if (ZoneAbstraitList[x, y] == null)
            {
                ZoneAbstraitList[x, y] = zoneAbstrait;
            }
        }

        public void RemovePersonnage(PersonnageAbstrait unPersonnage)
        {
            ZoneAbstraitList[unPersonnage.Position.Coordonne.X, unPersonnage.Position.Coordonne.Y].RetirerPersonnage(unPersonnage);
            var elemRemove = PersonnageList.Remove(unPersonnage);
            Console.WriteLine(elemRemove);
        }


        public virtual void ChargerEnvironnement(FabriqueAbstraite fabrique)
        {
            
        }

        public virtual void ChargerObjets(FabriqueAbstraite fabrique)
        {
            
        }

        public virtual void ChargerPersonnage(FabriqueAbstraite fabrique)
        {
            
        }

        public abstract void DeplacerPersonnage(PersonnageAbstrait unPersonnage, ZoneAbstraite zoneSource, ZoneAbstraite zoneFin);


        protected EnvironnementAbstrait(int dimensionX = 25, int dimensionY = 25)
        {
            PersonnageList = new ObservableCollection<PersonnageAbstrait>();
            ObjetsList = new List<ObjetAbstrait>();
            //ZoneAbstraitList = new List<ZoneAbstraite>();
            ZoneAbstraitList = new ZoneAbstraite[dimensionX, dimensionY];
            AccesAbstraitList = new List<AccesAbstrait>();
            Etapes = new List<Etape>();
            DimensionX = dimensionX;
            DimensionY = dimensionY;
        }

        public abstract Etape Simuler();

        public abstract string Statistiques();

        public virtual void Save(XmlNode rootNode, XmlWriterHelper helper)
        {
            helper.WriteAttribute(rootNode, "DimensionX", DimensionX.ToString());
            helper.WriteAttribute(rootNode, "DimensionY", DimensionY.ToString());

            XmlNode listNode = helper.XmlDocument.CreateElement("ZoneAbstraitList");
            for (int i = 0; i <= DimensionX - 1; i++)
            {
                for (int j = 0; j <= DimensionY - 1; j++)
                {
                    helper.WriteXmlData(listNode, "ZoneAbstrait", ZoneAbstraitList[i,j]);
                }
            }
            rootNode.AppendChild(listNode);
            helper.WriteListXmlData<Etape>(rootNode, "Etapes", "Etape", Etapes);
            helper.WriteAttribute(rootNode, "EtapeCount", EtapeCount.ToString());
        }

        public virtual void Hydrate(XmlNode rootNode, XmlReaderHelper helper)
        {
            DimensionX = int.Parse(helper.GetAttribute(rootNode, "DimensionX"));
            DimensionY = int.Parse(helper.GetAttribute(rootNode, "DimensionY"));

            PersonnageList = new ObservableCollection<PersonnageAbstrait>();
            List<ZoneAbstraite> zoneList = helper.GetListXmlDataFabriqueZone<ZoneAbstraite>(rootNode, "ZoneAbstraitList", "ZoneAbstrait");
            ZoneAbstraitList = new ZoneAbstraite[DimensionX, DimensionY];
            foreach (var zone in zoneList)
            {
                //ZoneAbstraitList[zone.Coordonne.X, zone.Coordonne.Y] = zone;
                AjouterZoneAbstrait(zone, zone.Coordonne.X, zone.Coordonne.Y);
                foreach (var personnage in zone.PersonnageList)
                {
                    personnage.Position = zone;
                    PersonnageList.Add(personnage);
                }
            }

            Etapes = helper.GetXmlListData<Etape>(rootNode, "Etapes");
            EtapeCount = int.Parse(helper.GetAttribute(rootNode, "EtapeCount"));
        }

        public virtual string GetNodeName()
        {
            throw new NotImplementedException();
        }
    }
}