﻿namespace LibAbstraite.GestionEnvironnement
{
    public abstract class AccesAbstrait
    {
        public ZoneAbstraite debut { get; set; }
        public ZoneAbstraite fin { get; set; }

        public AccesAbstrait(ZoneAbstraite debut, ZoneAbstraite fin)
        {
            this.debut = debut;
            this.fin = fin;
        }
    }
}