﻿using System;
using System.Xml;
using LibAbstraite.Xml;
using System.Collections.Generic;

namespace LibAbstraite.GestionEtape
{
    public class Etape : XmlData
    {
        public int Id { get; private set; }
        public List<EventAbstrait> Events { get; private set; }

        public Etape() : this(-1)
        {
            
        }

        public Etape(int Id)
        {
            this.Id = Id;
            Events = new List<EventAbstrait>();
        }

        public void Save(XmlNode rootNode, XmlWriterHelper helper)
        {
            helper.WriteAttribute(rootNode, "Id", Id.ToString());
            helper.WriteListXmlData(rootNode, "Events", "Event", Events);
        }

        public void Hydrate(XmlNode rootNode, XmlReaderHelper helper)
        {
            Id = int.Parse(helper.GetAttribute(rootNode, "Id"));
            Events = helper.GetListXmlDataFabriqueEvent<EventAbstrait>(rootNode, "Events", "Event");
        }

        public string GetNodeName()
        {
            return "Etape";
        }

        public void AddEvent(EventAbstrait Event) {
            Events.Add(Event);
        }
    }
}