﻿using System;
using System.Xml;
using LibAbstraite.Xml;

namespace LibAbstraite.GestionEtape
{
    public abstract class EventAbstrait : XmlData
    {
        public string NodeName
        {
            get { return GetNodeName(); }
        }
        public virtual string GetNodeName()
        {
            throw new NotImplementedException();
        }

        public virtual void Hydrate(XmlNode rootNode, XmlReaderHelper helper)
        {
            throw new NotImplementedException();
        }

        public virtual void Save(XmlNode rootNode, XmlWriterHelper helper)
        {
            throw new NotImplementedException();
        }

        public enum Type
        {
            MeteoEvent,
            DeadEvent,
            ObjetPickEvent,
            ObjetDropEvent,
            EatEvent,
            SpawnEvent
        }
    }
}