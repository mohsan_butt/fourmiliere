﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Windows.Data;
using LibAbstraite;
using LibAbstraite.GestionEnvironnement;
using LibAbstraite.GestionPersonnages;
using LibMetier.Fabriques;
using LibMetier.GestionEnvironnement;
using LibMetier.GestionObjets;
using LibMetier.GestionPersonnages;
using LibAbstraite.GestionEtape;
using System.Windows.Controls;
using System.Windows;
using LibAbstraite.GestionObjets;

namespace FourmiliereIHM
{
    public class FourmiliereViewModel : ViewModelBase
    {
        public int DimensionX { get; set; }
        public int DimensionY { get; set; }

        public Fourmiliere Fourmilier { get; set; }
        public FabriqueFourmiliere Fabrique { get; set; }

        public int VitesseExecution { get; set; }
  
        public bool EnCours { get; private set; }

        private string titreApplication;
        public string TitreApplication {
            get {
                return titreApplication;
                 }
            set
            {
                titreApplication = value;
                OnPropertyChanged("TitreApplication");
            }
                
        }
        private MTObservableCollection<Fourmi> fourmisList;
        public MTObservableCollection<Fourmi> FourmisList
        {
            get
            {
                return fourmisList;
            }
            set
            {
                fourmisList = value;
                OnPropertyChanged("FourmisList");
            }
        }

        private Fourmi fourmiSelect;
        public Fourmi FourmiSelect {
            get {
                return fourmiSelect;
            }
            set
            {
                etapeSelect = null;
                OnPropertyChanged("EtapeSelect");
                fourmiSelect = value;
                OnPropertyChanged("FourmiSelect");
            }
        }

        private MTObservableCollection<Etape> etapesList;
        public MTObservableCollection<Etape> EtapesList
        {
            get
            {
                return etapesList;
            }
            set
            {
                etapesList = value;
                OnPropertyChanged("EtapesList");
            }
        }

        private Etape etapeSelect;
        public Etape EtapeSelect
        {
            get
            {
                return etapeSelect;
            }
            set
            {
                fourmiSelect = null;
                OnPropertyChanged("FourmiSelect");
                etapeSelect = value;
                OnPropertyChanged("EtapeSelect");
            }
        }


        public FourmiliereViewModel()
        {
            TitreApplication = "Fourmiliere MOC 2";
            FourmisList = new MTObservableCollection<Fourmi>();
            EtapesList = new MTObservableCollection<Etape>();

         
            VitesseExecution = 2000;
            DimensionX = 25;
            DimensionY = 25;
            Fabrique = FabriqueFourmiliere.Instance;

            Fourmilier =
                new FourmiliereBuilder(Fabrique)
                .SetDimentionX(DimensionX)
                .SetDimentionY(DimensionY)
                .SetHome(new Position(2, 2))
                .Create();

            Ceuilleuse fourmi = (Ceuilleuse) Fabrique.CreePersonnage(PersonnageAbstrait.Type.CEUILLEUSE);
            Fourmilier.AjouterPersonage(fourmi);
            fourmisList.Add(fourmi);
            Fourmilier.PersonnageList[0].Position.ObjectList.Add(Fabrique.CreeObjet(LibAbstraite.GestionObjets.ObjetAbstrait.Type.OEUF));

            Fourmi f = (Fourmi) Fabrique.CreePersonnage(PersonnageAbstrait.Type.COMBATTANT);
            Fourmilier.AjouterPersonage(f);
            fourmisList.Add(f);
            BindingOperations.EnableCollectionSynchronization(FourmisList, this);
        }

        public void AjouterFourmi()
        {
            var hazard = new Random();
            var number = hazard.Next(0,3);
            if (number == 0)
            {
                var fourmi = Fabrique.CreePersonnage(PersonnageAbstrait.Type.FOURMI) as Fourmi;
                Fourmilier.AjouterPersonage(fourmi);
                fourmisList.Add(fourmi);
            }
            else if (number == 1)
            {
                var fourmi = Fabrique.CreePersonnage(PersonnageAbstrait.Type.CEUILLEUSE) as Ceuilleuse;
                Fourmilier.AjouterPersonage(fourmi);
                fourmisList.Add(fourmi);
            }
            else
            {
                var fourmi = Fabrique.CreePersonnage(PersonnageAbstrait.Type.COMBATTANT) as Combattant;
                Fourmilier.AjouterPersonage(fourmi);
                fourmisList.Add(fourmi);
            }
        }

        public void AjouterOeuf()
        {
            Fourmilier.AjouterObjet(Fabrique.CreeObjet(ObjetAbstrait.Type.OEUF));
        }

        public void RemoveFourmi()
        {
            Fourmilier.RemovePersonnage(FourmiSelect);
            FourmisList.Remove(FourmiSelect);
        }

        internal void TourSuivant()
        {
            Etape etape = Fourmilier.Simuler();
            EtapesList.Add(etape);
        }

        public void Avance()
        {
            EnCours = true;
            while (EnCours)
            {
                Thread.Sleep(VitesseExecution);
                TourSuivant();
            }
        }

        public void Stop()
        {
            EnCours = false;
        }

        private void ListBox_selectionChanged(object sender, DragEventArgs e)
        {
            e.Handled = true;
        }
    }
}