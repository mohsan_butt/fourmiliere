﻿using LibAbstraite;
using LibAbstraite.GestionEnvironnement;
using LibAbstraite.Xml;
using LibMetier.Fabriques;
using LibMetier.GestionEnvironnement;
using LibMetier.GestionPersonnages;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml;

namespace FourmiliereIHM
{

    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private static string FILTER_FILE = "XML files(*.xml)| *.xml|All files (*.*)|*.*";
        DispatcherTimer dt = new DispatcherTimer();
        Stopwatch stopWatch = new Stopwatch();

        public MainWindow()
        {
            InitializeComponent();
            DataContext = App.FourmiliereVM;

            dt.Tick += new EventHandler(Redessine_Tick);
            dt.Interval = new TimeSpan(0,0,0,1);

            DessinePlateau();
        }

        private void Redessine_Tick(object sender, EventArgs e)
        {
            if (stopWatch.IsRunning)
            {
                DessinePlateau();
            }
        }

        private void DessinePlateau()
        {

            Plateau.ColumnDefinitions.Clear();
            Plateau.RowDefinitions.Clear();
            Plateau.Children.Clear();
            
            for(int i = 0; i < App.FourmiliereVM.DimensionX; ++i)
            {
                Plateau.ColumnDefinitions.Add(new ColumnDefinition());
            }
            for(int i = 0; i < App.FourmiliereVM.DimensionY; ++i)
            {
                Plateau.RowDefinitions.Add(new RowDefinition());
            }

            //Ellipse e = new Ellipse();
            //e.Fill = new SolidColorBrush(Colors.RosyBrown);
            //Grid.SetColumn(e, 4);
            //Grid.SetRow(e, 4);
            //Plateau.Children.Add(e);

       
            for(int i = 0; i < App.FourmiliereVM.DimensionX; ++i)
            {
                for(int j = 0; j < App.FourmiliereVM.DimensionY; ++j)
                {
                    ZoneAbstraite zone = App.FourmiliereVM.Fourmilier.ZoneAbstraitList[i, j];

                    Button btn = new Button();
                    btn.Background = new SolidColorBrush(Colors.Azure);
                    AddUiToGrid(Plateau, btn, i, j);

                    btn.Click += BtnOnClick;
                    btn.Tag = zone.Nom;



                    if (zone.ObjectList.Count > 0)
                    {
                        Image img = new Image();
                        img.Source = new BitmapImage(new Uri("icon_egg.png", UriKind.Relative));
                        Plateau.Children.Add(img);
                        Grid.SetColumn(img, i);
                        Grid.SetRow(img, j);
                    }
                }
            }

            Image imgHill = new Image();
            imgHill.Source = new BitmapImage(new Uri("icon_ant_hill.png", UriKind.Relative));
            Plateau.Children.Add(imgHill);
            Grid.SetColumn(imgHill, App.FourmiliereVM.Fourmilier.Home.X);
            Grid.SetRow(imgHill, App.FourmiliereVM.Fourmilier.Home.Y);

            foreach (var fourmi in App.FourmiliereVM.Fourmilier.PersonnageList)
            {
                Image img = new Image();
                img.Source = new BitmapImage(new Uri("icon_ant.png", UriKind.Relative));
                Plateau.Children.Add(img);
                Grid.SetColumn(img, fourmi.Position.Coordonne.X);
                Grid.SetRow(img, fourmi.Position.Coordonne.Y);
            }
        }



        private void BtnOnClick(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            MessageBox.Show(btn.Tag.ToString());
        }

        private void BtnAddFourmis_Click(object sender, RoutedEventArgs e)
        {
            App.FourmiliereVM.AjouterFourmi();
            DessinePlateau();
        }

        private void BtnAddOeuf_Click(object sender, RoutedEventArgs e)
        {
            App.FourmiliereVM.AjouterOeuf();
            DessinePlateau();
        }

        private void BtnSuppr_Click(object sender, RoutedEventArgs e)
        {
            App.FourmiliereVM.RemoveFourmi();
            DessinePlateau();
        }

        private void showApropos_Click(object sender, RoutedEventArgs e)
        {
            //new AproposWindow().ShowDialog();
        }

        private void btnSave_Click(object sender, RoutedEventArgs e)
        {
            Fourmiliere toto2 = App.FourmiliereVM.Fourmilier;
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = FILTER_FILE;
            saveFileDialog.FilterIndex = 2;
            saveFileDialog.RestoreDirectory = true;
            Stream stream;
            if (saveFileDialog.ShowDialog() == true && (stream = saveFileDialog.OpenFile()) != null)
            {
                XmlWriterHelper helper = new XmlWriterHelper();
                XmlDocument xmlDoc = helper.CreateDocument();
                helper.WriteXmlData(xmlDoc, "Fourmiliere", App.FourmiliereVM.Fourmilier);
                helper.SaveAs(stream);
            }
        }

        private void btnLoad_Click(object sender, RoutedEventArgs e)
        {
            Stream myStream = null;
            OpenFileDialog openFileDialog = new OpenFileDialog();
            
            openFileDialog.Filter = FILTER_FILE;
            openFileDialog.FilterIndex = 2;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == true)
            {
                if ((myStream = openFileDialog.OpenFile()) != null)
                {
                    using (myStream)
                    {
                        XmlReaderHelper helper = new XmlReaderHelper();
                        XmlDocument XmlDoc = helper.Open(myStream, FabriqueFourmiliere.Instance);
                        Fourmiliere fourmiliere = helper.GetXmlDataFabriqueEnvironnement<Fourmiliere>(XmlDoc, "Fourmiliere");
                        App.FourmiliereVM.FourmisList.Clear();
                        foreach (var personnage in fourmiliere.PersonnageList) {
                            if (personnage is Fourmi && personnage.IsDead == false)
                            {
                                App.FourmiliereVM.FourmisList.Add((Fourmi) personnage);
                            }
                        }
                        App.FourmiliereVM.EtapesList.Clear();
                        foreach (var etape in fourmiliere.Etapes)
                        {
                            App.FourmiliereVM.EtapesList.Add(etape);
                        }
                        App.FourmiliereVM.Fourmilier = fourmiliere;
                        DessinePlateau();
                    }
                }
            }
        }

        private void btnQuit_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }


        private void test()
        {

        }


        // tour suivant
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            App.FourmiliereVM.TourSuivant();
            int x = App.FourmiliereVM.DimensionX;
            int y = App.FourmiliereVM.DimensionY;
            DessinePlateau();
        }

        private void Button_Avance(object sender, RoutedEventArgs e)
        {
            Thread tt = new Thread(App.FourmiliereVM.Avance);
            tt.Start();
            stopWatch.Start();
            dt.Start();
        }

        private void Button_Stop(object sender, RoutedEventArgs e)
        {
            App.FourmiliereVM.Stop();
            if (stopWatch.IsRunning)
            {
                stopWatch.Stop();
            }
            DessinePlateau();
        }

        private void AddUiToGrid(Grid uneGrille, UIElement unElement, int Column , int Row)
        {
            Plateau.Children.Add(unElement);
            Grid.SetColumn(unElement, Column);
            Grid.SetRow(unElement, Row);
        }

        private void ListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
