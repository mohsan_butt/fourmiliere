﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FourmiliereIHM
{
    public class AproposViewModel : ViewModelBase
    {

        public string Copyright { get { return "Mohsan"; } }

        public string DateApplication { get { return DateTime.Now.ToString(); } }

        public string Auteur { get { return "Mohsan BUTT";  } }
    }
}
