﻿using LibMetier.GestionEnvironnement;

namespace LibMetier
{
    public class Etape
    {
       public int Tour { get; set; }
       public BoutDeTerrain Lieu { get; set; } // Lieu == Zone
    }
}
