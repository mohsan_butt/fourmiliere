﻿using System;
using LibAbstraite.GestionEnvironnement;
using LibAbstraite.Observable;
using System.Xml;
using LibAbstraite;

namespace LibMetier.GestionEnvironnement
{
    public class BoutDeTerrain : ZoneAbstraite, IObserverMeteo
    {
        private Meteo Meteo { get; set; }


        public BoutDeTerrain(string unNom, int x, int y) : base(unNom, x , y)
        {
        }

        public BoutDeTerrain(string unNom) : base(unNom)
        {
            
        }

        public override void MediationConflit()
        {
            throw new System.NotImplementedException();
        }

        public void Update(Meteo meteo)
        {
            if (meteo.Equals(Meteo.Soleil))
            {
                meteo = Meteo;
            }

            foreach (var personnageAbstrait in PersonnageList)
            {
                personnageAbstrait.Update(meteo);
            }
        }

        public override string GetNodeName()
        {
            return ZoneAbstraite.Type.BOUTDETERRAIN.ToString();
        }
    }
}
