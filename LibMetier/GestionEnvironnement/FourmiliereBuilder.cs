﻿using LibAbstraite;
using LibAbstraite.GestionEnvironnement;
using LibMetier.Fabriques;
using System;

namespace LibMetier.GestionEnvironnement
{
    public class FourmiliereBuilder
    {
        private FabriqueFourmiliere Fabrique { get; set; }
        private int DimensionX { get; set; }
        private int DimensionY { get; set; }

        private Position Home { get; set; }

        public FourmiliereBuilder(FabriqueFourmiliere Fabrique)
        {
            this.Fabrique = Fabrique;
        }


        public FourmiliereBuilder SetDimentionX(int dimensionX)
        {
            this.DimensionX = dimensionX;
            return this;
        }

        public FourmiliereBuilder SetDimentionY(int dimensionY)
        {
            this.DimensionY = dimensionY;
            return this;
        }

        public FourmiliereBuilder SetHome(Position home)
        {
            this.Home = home;
            return this;
        }

        public Fourmiliere Create()
        {
            if (Home == null)
            {
                var hasard = new Random();
                int x = hasard.Next(DimensionX);
                int y = hasard.Next(DimensionY);
                Home = new Position(x, y);
            }

            Fourmiliere Fourmiliere = Fabrique.CreerEnvironment() as Fourmiliere;

            Fourmiliere.DimensionX = DimensionX;
            Fourmiliere.DimensionY = DimensionY;

            for (int i = 0; i < DimensionX; ++i)
            {
                for (int j = 0; j < DimensionY; ++j)
                {
                    Fourmiliere.AjouterZoneAbstrait(
                        (i == Home.X && j == Home.Y) ?
                            Fabrique.CreeZone(ZoneAbstraite.Type.ZONEFOURMILIERE, "Fourmiliere X :" + i + " Y :" + j) :
                            Fabrique.CreeZone(ZoneAbstraite.Type.BOUTDETERRAIN, "X :" + i + " Y :" + j),
                        i, j);
                }
            }
            return Fourmiliere;
        }
    }
}