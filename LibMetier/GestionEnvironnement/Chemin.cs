﻿using LibAbstraite.Fabriques;
using LibAbstraite.GestionEnvironnement;

namespace LibMetier.GestionEnvironnement
{
    public class Chemin : AccesAbstrait
    {
        public Chemin(ZoneAbstraite debut, ZoneAbstraite fin) : base(debut, fin)
        {
        }
    }
}
