﻿using System;
using System.Collections.Generic;
using LibAbstraite.GestionEnvironnement;
using LibAbstraite.GestionObjets;
using LibAbstraite.GestionPersonnages;
using LibAbstraite.Observable;
using LibMetier.Fabriques;
using LibMetier.GestionPersonnages;

namespace LibMetier.GestionEnvironnement
{
    public class ZoneFourmiliere : BoutDeTerrain
    {

        public Reine Queen { get; set; }

        public Fourmiliere Fourmiliere { get; set; }

        public ZoneFourmiliere(string unNom) : base(unNom)
        {
            IsZoneFourmiliere = true;
            Queen = (Reine)FabriqueFourmiliere.Instance.CreePersonnage(PersonnageAbstrait.Type.REINE);
            Queen.SetZoneFourmiliere(this);
        }

        public ZoneFourmiliere(string unNom, int x, int y) : base(unNom, x , y)
        {
            IsZoneFourmiliere = true;
            Queen = (Reine) FabriqueFourmiliere.Instance.CreePersonnage(PersonnageAbstrait.Type.REINE);
            Queen.SetZoneFourmiliere(this);
        }

        public void DeposerObjet(ObjetAbstrait objet)
        {
           Queen.EatFood(objet);
        }

        public override string GetNodeName()
        {
            return ZoneAbstraite.Type.ZONEFOURMILIERE.ToString();
        }
    }
}
