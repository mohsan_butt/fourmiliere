﻿using System;
using System.CodeDom;
using LibAbstraite.Fabriques;
using LibAbstraite.GestionEnvironnement;
using LibAbstraite.GestionPersonnages;
using LibAbstraite.Observable;
using LibAbstraite.GestionEtape;
using LibMetier.Fabriques;
using LibMetier.GestionPersonnages;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml;
using LibAbstraite;
using LibAbstraite.Xml;
using LibMetier.GestionEtape;

namespace LibMetier.GestionEnvironnement
{
    public class Fourmiliere : EnvironnementAbstrait
    {

        public MeteoData MeteoObj { get; private set; }

        public Fourmiliere() : base()
        {
        }

        public override void DeplacerPersonnage(PersonnageAbstrait unPersonnage, ZoneAbstraite zoneSource, ZoneAbstraite zoneFin)
        {
            zoneSource.RetirerPersonnage(unPersonnage);
            zoneFin.AjouterPersonnage(unPersonnage);
            unPersonnage.Position = zoneFin;
            unPersonnage.AnalyseSituation();
        }

        public override Etape Simuler()
        {
            Etape etape = new Etape(++EtapeCount);
            if (MeteoObj == null)
            {
                MeteoObj = new MeteoData(ZoneAbstraitList);
            }

            Meteo newMeteo;
            Random random = new Random();
            if (random.Next(4) == 2)
            {
                newMeteo = Meteo.Orage;
            }
            else
            {
                newMeteo = Meteo.Soleil;
            }

            if (MeteoObj.Meteo != newMeteo)
            {
                etape.AddEvent(((MeteoEvent)FabriqueFourmiliere.Instance.CreeEvent(EventAbstrait.Type.MeteoEvent)).SetMeteo(newMeteo));
            }
            MeteoObj.SetMeteo(newMeteo);

            List<PersonnageAbstrait> morts = new List<PersonnageAbstrait>();
            foreach (var personnageAbstrait in PersonnageList)
            {
                if (personnageAbstrait is Fourmi fourmi)
                {
                    if (fourmi.IsDead)
                    {
                        morts.Add(fourmi);
                        etape.AddEvent(
                            ((DeadEvent) FabriqueFourmiliere.Instance.CreeEvent(EventAbstrait.Type.DeadEvent))
                            .SetPersonnage(fourmi));
                    }
                    else
                    {
                        AnanlyseAndMove(fourmi, etape);
                        fourmi.BaisserSante();
                    }
                }
            }
            foreach (var mort in morts)
            {
                PersonnageList.Remove(mort);
            }
            Etapes.Add(etape);
            return etape;
        }

        private void AnanlyseAndMove(Fourmi fourmi, Etape etape)
        {
            List<ZoneAbstraite> zones = new List<ZoneAbstraite>();
            int visibleRange = 1;
            if (fourmi.Position == null) return;
            LibAbstraite.Position currentPosition = fourmi.Position?.Coordonne;
            int minX = Math.Max(0, currentPosition.X - visibleRange);
            int maxX = Math.Min(DimensionX - 1, currentPosition.X + visibleRange);

            int minY = Math.Max(0, currentPosition.Y - visibleRange);
            int maxY = Math.Min(DimensionY - 1, currentPosition.Y + visibleRange);
            for (int i = minX; i <= maxX; i++)
            {
                for (int j = minY; j <= maxY; j++)
                {
                    zones.Add(ZoneAbstraitList[i, j]);
                }
            }
            ZoneAbstraite nextZone = fourmi.ChoixZoneSuivante(zones, etape);
            DeplacerPersonnage(fourmi, fourmi.Position, nextZone);
            fourmi.Execution(Home, etape);
        }

        public override void AjouterZoneAbstrait(ZoneAbstraite zoneAbstrait, int x, int y)
        {
            base.AjouterZoneAbstrait(zoneAbstrait, x, y);
            if (zoneAbstrait is ZoneFourmiliere)
                Home = zoneAbstrait.Coordonne;
        }

        public override string Statistiques()
        {
            return "Statisitque";
        }

        public void ShowMap()
        {
            for (int j = 0; j < ZoneAbstraitList.GetLength(0); j += 1)
            {
                for (int k = 0; k < ZoneAbstraitList.GetLength(1); k += 1)
                {
                    Console.Write(ZoneAbstraitList[j, k].Nom);
                }

                Console.WriteLine("");
            }
        }

        public override string ToString()
        {
            return "Personnage number : " + PersonnageList.Count.ToString();
        }

        public override void Save(XmlNode rootNode, XmlWriterHelper helper)
        {
            base.Save(rootNode, helper);
            helper.WriteAttribute(rootNode, "Meteo", MeteoObj.Meteo.ToString());
        }

        public override void Hydrate(XmlNode rootNode, XmlReaderHelper helper)
        {
            base.Hydrate(rootNode, helper);
            MeteoObj = new MeteoData(ZoneAbstraitList);
            MeteoObj.SetMeteo(helper.GetAttributeAsEnum<Meteo>(rootNode, "Meteo"));
        }


        public override string GetNodeName()
        {
            return "fourmiliere";
        }
    }

}
