﻿using System;
using LibAbstraite.Fabriques;
using LibAbstraite.GestionEnvironnement;
using LibAbstraite.GestionObjets;
using LibAbstraite.GestionPersonnages;
using LibMetier.GestionEnvironnement;
using LibMetier.GestionObjets;
using LibMetier.GestionPersonnages;
using LibMetier.GestionEtape;
using LibAbstraite.GestionEtape;

namespace LibMetier.Fabriques
{
    public class FabriqueFourmiliere : FabriqueAbstraite
    {
        public override string Title { get; }

        private static FabriqueFourmiliere _instance;

        private FabriqueFourmiliere() { }

        public static FabriqueFourmiliere Instance => _instance ?? (_instance = new FabriqueFourmiliere());


        public override AccesAbstrait CreeAccees(ZoneAbstraite zdebut, ZoneAbstraite zfin)
        {
            return new Chemin(new BoutDeTerrain("premier", 1,1), new BoutDeTerrain("second",1,2));
        }

        public override ObjetAbstrait CreeObjet(ObjetAbstrait.Type type)
        {
            switch (type)
            {
                case ObjetAbstrait.Type.OEUF:
                    return new Oeuf();

                case ObjetAbstrait.Type.NOURRITURE:
                    return new Nourriture();

                case ObjetAbstrait.Type.PHEROMONE:
                    return new Pheromone();
            }

            return null;
        }

        public override PersonnageAbstrait CreePersonnage(PersonnageAbstrait.Type type)
        {
            switch (type)
            {
                case PersonnageAbstrait.Type.FOURMI:
                    return new Fourmi();

                case PersonnageAbstrait.Type.REINE:
                    return new Reine();

                case PersonnageAbstrait.Type.CEUILLEUSE:
                    return new Ceuilleuse();

                case PersonnageAbstrait.Type.COMBATTANT:
                    return new Combattant();
            }

            return null;
        }

        public override EnvironnementAbstrait CreerEnvironment()
        {
            return new Fourmiliere();

        }

        public override ZoneAbstraite CreeZone(ZoneAbstraite.Type type, string name)
        {
            switch (type)
            {
                case ZoneAbstraite.Type.BOUTDETERRAIN:
                    return new BoutDeTerrain(name);

                case ZoneAbstraite.Type.ZONEFOURMILIERE:
                    return new ZoneFourmiliere(name);
            }

            return null;
        }

        public override EventAbstrait CreeEvent(EventAbstrait.Type type)
        {
            switch (type)
            {
                case EventAbstrait.Type.MeteoEvent:
                    return new MeteoEvent();

                case EventAbstrait.Type.DeadEvent:
                    return new DeadEvent();

                case EventAbstrait.Type.ObjetPickEvent:
                    return new ObjetPickEvent();

                case EventAbstrait.Type.ObjetDropEvent:
                    return new ObjetDropEvent();

                case EventAbstrait.Type.EatEvent:
                    return new EatEvent();

                case EventAbstrait.Type.SpawnEvent:
                    return new SpawnEvent();
            }

            return null;
        }
    }
}
