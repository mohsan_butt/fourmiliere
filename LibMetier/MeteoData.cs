﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibAbstraite.GestionEnvironnement;
using LibAbstraite.Observable;
using LibMetier.GestionEnvironnement;

namespace LibMetier
{
    public class MeteoData
    {
        public Meteo Meteo { get; set; }
        public ZoneAbstraite[,] ListBoutDeTerrains { get; set; }
        public MeteoData(ZoneAbstraite[,] listBoutDeTerrain, Meteo meteo = Meteo.Soleil)
        {
            Meteo = meteo;
            ListBoutDeTerrains = listBoutDeTerrain;
        }

        public MeteoData()
        {
        }

        public void SetMeteo(Meteo meteo)
        {
            Meteo = meteo;
            for (int j = 0; j < ListBoutDeTerrains.GetLength(0); j += 1)
            {
               for (int k = 0; k < ListBoutDeTerrains.GetLength(1); k += 1)
               {
                   var boutdeterrain = ListBoutDeTerrains[j, k] as BoutDeTerrain;
                   boutdeterrain?.Update(meteo);
               }
        
              
            }   
        }
    }
}
