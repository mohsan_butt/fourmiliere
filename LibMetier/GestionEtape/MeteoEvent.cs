﻿using LibAbstraite.GestionEtape;
using LibAbstraite.Observable;
using LibAbstraite.Xml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace LibMetier.GestionEtape
{
    class MeteoEvent : EventAbstrait
    {

        private Meteo Meteo { get; set; }
        public override void Save(XmlNode rootNode, XmlWriterHelper helper)
        {
            helper.WriteAttribute(rootNode, "Meteo", Meteo.ToString());
        }

        public override void Hydrate(XmlNode rootNode, XmlReaderHelper helper)
        {
            Meteo = helper.GetAttributeAsEnum<Meteo>(rootNode, "Meteo");
        }

        public override string GetNodeName()
        {
            return EventAbstrait.Type.MeteoEvent.ToString();
        }

        public MeteoEvent SetMeteo(Meteo Meteo)
        {
            this.Meteo = Meteo;
            return this;
        }
    }
}
