﻿using LibAbstraite.GestionEtape;
using LibAbstraite.Xml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using LibAbstraite.GestionObjets;
using LibAbstraite.GestionPersonnages;

namespace LibMetier.GestionEtape
{
    class EatEvent : EventAbstrait
    {
        private PersonnageAbstrait Personnage;
        private ObjetAbstrait Objet;

        public override void Save(XmlNode rootNode, XmlWriterHelper helper)
        {
            helper.WriteXmlData<PersonnageAbstrait>(rootNode, "Personnage", Personnage);
            helper.WriteXmlData<ObjetAbstrait>(rootNode, "Objet", Objet);
        }

        public override void Hydrate(XmlNode rootNode, XmlReaderHelper helper)
        {
            Personnage = helper.GetXmlDataFabriquePersonnage<PersonnageAbstrait>(rootNode, "Personnage");
            Objet = helper.GetXmlDataFabriqueObjet<ObjetAbstrait>(rootNode, "Objet");
        }

        public override string GetNodeName()
        {
            return EventAbstrait.Type.EatEvent.ToString();
        }

        public EatEvent SetPersonnage(PersonnageAbstrait Personnage)
        {
            this.Personnage = Personnage;
            return this;
        }

        public EatEvent SetObjet(ObjetAbstrait Objet)
        {
            this.Objet = Objet;
            return this;
        }
    }
}
