﻿using LibAbstraite.GestionEtape;
using LibAbstraite.GestionPersonnages;
using LibAbstraite.Xml;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace LibMetier.GestionEtape
{
    class DeadEvent : EventAbstrait
    {
        private PersonnageAbstrait Personnage;
        public override void Save(XmlNode rootNode, XmlWriterHelper helper)
        {
            helper.WriteXmlData<PersonnageAbstrait>(rootNode, "Personnage", Personnage);
        }

        public override void Hydrate(XmlNode rootNode, XmlReaderHelper helper)
        {
            Personnage = helper.GetXmlDataFabriquePersonnage<PersonnageAbstrait>(rootNode, "Personnage");
        }

        public override string GetNodeName()
        {
            return EventAbstrait.Type.DeadEvent.ToString();
        }

        public DeadEvent SetPersonnage(PersonnageAbstrait Personnage)
        {
            this.Personnage = Personnage;
            return this;
        }
    }
}
