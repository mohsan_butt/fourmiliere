﻿using LibAbstraite.Fabriques;
using LibAbstraite.GestionObjets;

namespace LibMetier.GestionObjets
{
   public class Nourriture : ObjetAbstrait
    {
        public Nourriture()
        {
            Nom = "Nourriture";
        }

        public override string GetNodeName()
        {
            return ObjetAbstrait.Type.NOURRITURE.ToString();
        }
    }
}
