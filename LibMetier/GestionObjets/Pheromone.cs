﻿using LibAbstraite.GestionObjets;

namespace LibMetier.GestionObjets
{
    public class Pheromone : ObjetAbstrait
    {
        public Pheromone()
        {
            Nom = "pheromone";
        }

        public override string GetNodeName()
        {
            return ObjetAbstrait.Type.PHEROMONE.ToString();
        }
    }
}
