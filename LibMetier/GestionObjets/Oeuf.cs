﻿using LibAbstraite.GestionObjets;

namespace LibMetier.GestionObjets
{
    public class Oeuf : ObjetAbstrait
    {
        public Oeuf()
        {
            Nom = "Oeuf";
        }

        public override string GetNodeName()
        {
            return ObjetAbstrait.Type.OEUF.ToString();
        }
    }
}
