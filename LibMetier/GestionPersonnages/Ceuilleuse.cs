﻿using System.Collections.Specialized;
using LibAbstraite.GestionEnvironnement;
using LibAbstraite.Observable;
using LibAbstraite.Strategy;
using LibMetier.GestionEnvironnement;
using LibMetier.Strategy;
using LibAbstraite.GestionPersonnages;

namespace LibMetier.GestionPersonnages
{
    public class Ceuilleuse : Fourmi
    {

 

        public Ceuilleuse() : this("Ceulleuse")
        {
        }


        public Ceuilleuse(string name) : base(name)
        {
            Behavior = new ComportementCeuilleuse(this);
            FindPath = new DeplacementCeuilleuse(this);
            Sante = 150;
        }

        public override void Update(Meteo meteo)
        {
            meteo = Meteo;
            if (meteo == Meteo.Orage)
            {
                FindPath = new NoDeplacement(this);
            }

            if (meteo == Meteo.Soleil)
            {
                FindPath = new DeplacementCeuilleuse(this);
            }
        }

        public override string GetNodeName()
        {
            return PersonnageAbstrait.Type.CEUILLEUSE.ToString();
        }
    }
}