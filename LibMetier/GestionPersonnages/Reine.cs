﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.Linq;
using LibAbstraite.Fabriques;
using LibAbstraite.GestionEnvironnement;
using LibAbstraite.GestionPersonnages;
using LibAbstraite.Observable;
using LibMetier.Fabriques;
using LibMetier.GestionEnvironnement;
using System.Xml;
using LibAbstraite;
using LibAbstraite.GestionObjets;
using LibAbstraite.Xml;
using LibAbstraite.GestionEtape;

namespace LibMetier.GestionPersonnages
{
    public class Reine : Fourmi
    {
        public ZoneFourmiliere Fourmiliere { get; set; }
        public int NumberFood { get; set; }
        public bool Naissance { get; set; }

        public Reine()
        {
            Nom = "Reine";
            Sante = 100;
        }
        public override String ToString()
        {
            return "Reine";
        }

        public override ZoneAbstraite ChoixZoneSuivante(List<ZoneAbstraite> accesList, Etape Etape)
        {
            return Position;
        }

        public override void AnalyseSituation()
        {
            var hasard = new Random();
            Naissance = (hasard.Next() % 2 == 0);
        }

        public override void Execution(Position Home, Etape Etape)
        {
           
        }

        public override void Save(XmlNode rootNode, XmlWriterHelper helper)
        {
            base.Save(rootNode, helper);
            helper.WriteAttribute(rootNode, "Naissance", Naissance ? "1" : "0");
        }

        public override string GetNodeName()
        {
            return PersonnageAbstrait.Type.REINE.ToString();
        }

        public override void Update(Meteo meteo)
        {
            if (meteo == Meteo.Orage)
            {
                Console.WriteLine("Can not naitre ant");
            }
        }

        public void EatFood(ObjetAbstrait food)
        {
            NumberFood++;

            FabriqueFourmiliere fabrique = FabriqueFourmiliere.Instance;
            
            if (NumberFood > 6) { 
                Fourmiliere.AjouterPersonnage(fabrique.CreePersonnage(Type.COMBATTANT));
                NumberFood -= 3;
            } else if (NumberFood >= 3)
            {
                Fourmiliere.AjouterPersonnage(fabrique.CreePersonnage(Type.CEUILLEUSE));
                NumberFood -= 3;
            }
        }

        public void SetZoneFourmiliere(ZoneFourmiliere zoneFourmiliere)
        {
           Fourmiliere = zoneFourmiliere;
        }
    }
}
