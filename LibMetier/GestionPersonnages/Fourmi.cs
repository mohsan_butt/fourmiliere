﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using LibAbstraite.Fabriques;
using LibAbstraite.GestionEnvironnement;
using LibAbstraite.GestionPersonnages;
using LibAbstraite.Observable;
using LibAbstraite.Strategy;
using LibMetier.GestionEnvironnement;
using LibMetier.GestionObjets;
using LibMetier.Strategy;
using System.Xml;
using LibAbstraite;
using LibAbstraite.Xml;
using LibAbstraite.GestionEtape;

namespace LibMetier.GestionPersonnages
{

    public class Fourmi : PersonnageAbstrait
    {

        public Fourmi() : this("Anonyme")
        {
        }


        public Fourmi(string name) : base(name) 
        {
            FindPath = new DeplacementNormal(this);
            Sante = 200;
        }


        public override ZoneAbstraite ChoixZoneSuivante(List<ZoneAbstraite> accesList, Etape Etape)
        {
            ZoneAbstraite nextPath = FindPath.NextPath(accesList, Etape);
            if (nextPath == null)
            {
                nextPath = accesList[new Random().Next(1, accesList.Count())];
            }

            return nextPath;
        }

        public override void AnalyseSituation()
        {
            var listPersonnage = Position.PersonnageList;
            var listObject = Position.ObjectList;

            /*if (listPersonnage.Count > 0)
            {
                Console.WriteLine("Personnage dans la zone");
            }*/

            var objetAbstrait = listObject.SingleOrDefault(element => element is Oeuf);

            /*if (objetAbstrait != null)
            {
                Console.WriteLine("Find Oeuf");
            }*/

            this.BaisserSante();
        }


        public override void Execution(Position Home, Etape Etape)
        {
            Behavior.ExecuteObject(Position, Home, Etape);
            Behavior.ExecuteFourmi(Etape);
        }

        /*public void AvanceUnTour(int dimX, int dimY)
        {
            AvanceHasard(dimX, dimY);
            ListEtape.Add(new Etape());
            this.BaisserSante();
        }

        private void AvanceHasard(int dimX, int dimY)
        {
            Random Hazard = new Random();

            int x = Position.Coordonne.X;
            int y = Position.Coordonne.Y;

            int newX = x + Hazard.Next(3) - 1; // - 1 , 0 , 1
            int newY = y + Hazard.Next(3) - 1; // 

            if ((newX >= 0) && (newX < dimX))
            {
                x = newX;
            }

            if ((newY >= 0) && (newY < dimY))
            {
                y = newY;
            }


           _fourmiliereManger.DeplacerPersonnage(this, this.Position,
                    _fourmiliereManger.ZoneAbstraitList[x, y]);
        }*/

        public override void Update(Meteo meteo)
        {
            Meteo = meteo;
            if (meteo == Meteo.Orage)
            {
                FindPath = new NoDeplacement(this);
            }

            if (meteo == Meteo.Soleil)
            {
                FindPath = new DeplacementNormal(this);
            }
        }

        public override String ToString()
        {
            return "Fourmi";
        }

        public override string GetNodeName()
        {
            return PersonnageAbstrait.Type.FOURMI.ToString();
        }
    }
}
