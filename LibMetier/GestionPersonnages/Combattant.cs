﻿using LibAbstraite.GestionPersonnages;
using LibAbstraite.Observable;
using LibAbstraite.Strategy;
using LibMetier.Strategy;

namespace LibMetier.GestionPersonnages
{
    public class Combattant : Fourmi
    {

        public Combattant() : this("Combattant")
        {
        }

        public Combattant(string name) : base(name)
        {
            Behavior = new ComportementCombattant(this);
            FindPath = new DeplacementCombattant(this);
            Sante = 300;
        }

        public override void Update(Meteo meteo)
        {
            meteo = Meteo;

        }

        public override string GetNodeName()
        {
            return PersonnageAbstrait.Type.COMBATTANT.ToString();
        }

    }


}