﻿using System;
using System.Collections.Generic;
using LibAbstraite.GestionEnvironnement;
using LibAbstraite.GestionPersonnages;
using LibAbstraite.Strategy;
using System;
using LibAbstraite.GestionEtape;

namespace LibMetier.Strategy
{
    internal class DeplacementCombattant : Deplacement
    {
        public DeplacementCombattant(PersonnageAbstrait personnageAbstrait) : base(personnageAbstrait)
        {
        }

        public override ZoneAbstraite NextPath(List<ZoneAbstraite> zones, Etape Etape)
        {
            foreach (var zone in zones)
            {
                if (Personnage.Position.Coordonne.X == zone.Coordonne.X && Personnage.Position.Coordonne.Y == zone.Coordonne.Y)
                    continue;
                if (zone.PersonnageList.Count > 0)
                {
                    return zone;
                }
            }
            var hasard = new Random();
            return zones[hasard.Next(zones.Count - 1)];
        }
    }
}