﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibAbstraite;
using LibAbstraite.GestionEnvironnement;
using LibAbstraite.GestionObjets;
using LibAbstraite.GestionPersonnages;
using LibAbstraite.Strategy;
using LibMetier.GestionEnvironnement;
using LibMetier.GestionObjets;
using LibAbstraite.GestionEtape;
using LibMetier.Fabriques;
using LibMetier.GestionEtape;

namespace LibMetier.Strategy
{
    public class ComportementCeuilleuse : Comportement
    {
        public ComportementCeuilleuse(PersonnageAbstrait personnageAbstrait) : base(personnageAbstrait)
        {
        }

        public override void ExecuteObject(ZoneAbstraite zone, Position Home, Etape Etape)
        {

            if (zone.IsZoneFourmiliere && Personnage.ListStock.Count > 0)
            {
                ZoneFourmiliere zoneFourmiliere = (ZoneFourmiliere) zone;
                ObjetAbstrait objet = Personnage.ListStock[0];
                zoneFourmiliere.DeposerObjet(objet);
                Personnage.ListStock.Remove(objet);
                Etape.AddEvent(((ObjetDropEvent)FabriqueFourmiliere.Instance.CreeEvent(EventAbstrait.Type.ObjetDropEvent))
                    .SetPersonnage(Personnage)
                    .SetObjet(objet));

                Etape.AddEvent(((EatEvent)FabriqueFourmiliere.Instance.CreeEvent(EventAbstrait.Type.EatEvent))
                    .SetPersonnage(Personnage)
                    .SetObjet(objet));
            }
            else
            {
                foreach (var objet in zone.ObjectList)
                {
                    if (objet is Oeuf)
                    {
                        Personnage.ListStock.Add((Oeuf)objet);
                        zone.ObjectList.Remove((Oeuf)objet);
                        Personnage.NumberOfElementFind = Personnage.NumberOfElementFind  + 1;
                        Personnage.Destination = Home;

                        Etape.AddEvent(((ObjetPickEvent)FabriqueFourmiliere.Instance.CreeEvent(EventAbstrait.Type.ObjetPickEvent))
                            .SetPersonnage(Personnage)
                            .SetObjet(objet));
                        break;
                    }

                }
            }
        }
    }
}
