﻿using LibAbstraite.GestionEtape;
using LibAbstraite.GestionPersonnages;
using LibAbstraite.Strategy;

namespace LibMetier.Strategy
{
    public class ComportementCombattant : Comportement
    {
        public ComportementCombattant(PersonnageAbstrait personnageAbstrait) : base(personnageAbstrait)
        {
        }

        public override void ExecuteFourmi(Etape Etape)
        {
            foreach (var personnage in Personnage.Position.PersonnageList)
            {
                if (personnage != Personnage)
                {
                    personnage.Sante -= 10;
                    Personnage.NumberOfAttack = Personnage.NumberOfAttack + 1;
                }
            }
        }
    }
}