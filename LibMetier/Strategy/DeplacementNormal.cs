﻿using System;
using System.Collections.Generic;
using LibAbstraite.GestionEnvironnement;
using LibAbstraite.GestionPersonnages;
using LibAbstraite.Strategy;
using LibAbstraite.GestionEtape;

namespace LibMetier.Strategy
{
    public class DeplacementNormal : Deplacement
    {
        public DeplacementNormal(PersonnageAbstrait personnageAbstrait) : base(personnageAbstrait)
        {
        }

        public override ZoneAbstraite NextPath(List<ZoneAbstraite> zones, Etape Etape)
        {
            var random = new Random();
            return zones[random.Next(zones.Count)];
        }
    }
}