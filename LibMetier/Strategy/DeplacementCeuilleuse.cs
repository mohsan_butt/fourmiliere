﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibAbstraite.GestionEnvironnement;
using LibAbstraite.GestionPersonnages;
using LibAbstraite.Strategy;
using LibAbstraite.GestionEtape;

namespace LibMetier.Strategy
{
    class DeplacementCeuilleuse : Deplacement
    {
        public DeplacementCeuilleuse(PersonnageAbstrait personnageAbstrait) : base(personnageAbstrait)
        {
        }

        public override ZoneAbstraite NextPath(List<ZoneAbstraite> zones, Etape Etape)
        {
            if (zones.Count == 0) return null; 
            ZoneAbstraite zoneDestination = null;
            ZoneAbstraite zoneDirectionDestination = null;
            ZoneAbstraite zoneFood = null;

            int currentDestX = 0;
            int currentDestY = 0;
            if (Personnage.Destination != null)
            {
                zoneDirectionDestination = zones[0];
                currentDestX = Math.Abs(Personnage.Destination.X - zones[0].Coordonne.X);
                currentDestY = Math.Abs(Personnage.Destination.Y - zones[0].Coordonne.Y);
            }
            foreach (var zone in zones)
            {
                if (zone.ObjectList.Count > 0 && (zoneFood == null || zone.ObjectList.Count > zoneFood.ObjectList.Count))
                {
                    zoneFood = zone;
                }

                if (Personnage.Destination != null)
                {
                    if (zone.Coordonne.X == Personnage.Destination.X && zone.Coordonne.Y == Personnage.Destination.Y)
                    {
                        zoneDestination = zone;
                        continue;
                    }

                    int destX = Math.Abs(Personnage.Destination.X - zone.Coordonne.X);
                    int destY = Math.Abs(Personnage.Destination.Y - zone.Coordonne.Y);

                    if (destX < currentDestX || destY < currentDestY)
                    {
                        zoneDirectionDestination = zone;
                        currentDestX = destX;
                        currentDestY = destY;
                    }
                }
            }

            if (Personnage.ListStock.Count > 0) // Go Home
            {
                if (zoneDestination != null)
                    return zoneDestination;

                if (zoneDirectionDestination != null)
                    return zoneDirectionDestination;
            }
            if (zoneFood != null)
                return zoneFood;

            return zones[new Random().Next(zones.Count - 1)];
        }

       
    }
}
