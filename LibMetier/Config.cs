﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LibMetier
{
    public class Config
    {

        public static readonly string ApplicationTitle = "Fourmiliere";
   
        public static readonly int GrilleLargeur = 20;
        public static readonly int GrilleHauteur = 30;

        public static readonly int VitesseExecution = 500;
    }
}
