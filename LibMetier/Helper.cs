﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibMetier.GestionEnvironnement;
using System.Security.Cryptography;

namespace LibMetier
{
    public class Helper
    {
        public static string GetUniqueKey()
        {
            int size = 4;
            byte[] data = new byte[size];
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            crypto.GetBytes(data);
            return BitConverter.ToString(data).Replace("-", String.Empty);
        }
    }
}
