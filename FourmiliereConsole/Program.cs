﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibAbstraite.Observable;
using LibMetier;
using LibMetier.Fabriques;
using LibMetier.GestionEnvironnement;
using LibMetier.GestionPersonnages;
using LibAbstraite.GestionPersonnages;
using LibAbstraite;

namespace FourmiliereConsole
{
    class Program
    {

        static void Main(string[] args)
        {
            int DimensionX = 25;
            int DimensionY = 25;

            FabriqueFourmiliere fabriq = FabriqueFourmiliere.Instance;

            Fourmiliere fourmiliere =
                new FourmiliereBuilder(fabriq)
                .SetDimentionX(DimensionX)
                .SetDimentionY(DimensionY)
                .SetHome(new Position(2, 2))
                .Create();

            var reine = fabriq.CreePersonnage(PersonnageAbstrait.Type.REINE) as Reine;
            var fourmi = fabriq.CreePersonnage(PersonnageAbstrait.Type.CEUILLEUSE);

            MeteoData meteo = new MeteoData();
            fourmiliere.ShowMap();

            Console.WriteLine(reine.Nom);

            //fourmiliere.AjouterPersonage(reine);
            fourmiliere.AjouterPersonage(fourmi);

            Console.WriteLine("Number personne " + fourmiliere.ToString());
            Console.WriteLine("Toto" + fourmi.Position.Nom);
            fourmi.Position.ObjectList.Add(fabriq.CreeObjet(LibAbstraite.GestionObjets.ObjetAbstrait.Type.OEUF));
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine("Postion Four mi X " + fourmi.Position.Coordonne.X.ToString() + "Y :  " + fourmi.Position.Coordonne.Y.ToString());
                fourmiliere.Simuler();
                Console.WriteLine("Postion Four mi X " + fourmi.Position.Coordonne.X.ToString() + "Y :  " + fourmi.Position.Coordonne.Y.ToString());
            }

            meteo.ListBoutDeTerrains = fourmiliere.ZoneAbstraitList;
            meteo.SetMeteo(Meteo.Orage);
            meteo.SetMeteo(Meteo.Neige);

            Console.ReadKey();
        }
    }
}
